//
//  NetworkHelper.swift
//  NewsFeed
//
//  Created by Venkatesh on 3/13/16.
//  Copyright © 2016 venkatesh. All rights reserved.
//

import Foundation
import UIKit
class NetworkHelper
{
func fetchData(url: NSURL, onSuccess:((data: AnyObject) -> Void)?, onFail:((message:String) -> Void)?)
{    let request = NSMutableURLRequest(URL: url)
    request.HTTPMethod = "GET"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
       
        if error == nil{
            let res = response as! NSHTTPURLResponse!
            
            switch res.statusCode {
            case 200 :
                fallthrough
                
            case 201 :
                fallthrough
            case 204 :
                if data != nil {
                    do{
                        
                        
                        onSuccess!(data: data!)
                        return
                    }
                    catch{
                        onSuccess!(data: data!)
                    }
                }
                else{
                    onSuccess!(data: [])
                    return
                }
                
            case 401 :
                onFail!(message: "Invalid Credentials")
                return
            case 400:
                onFail!(message: "Incorrect Request Format")
                return
            default :
                onFail!(message : "Internal Server Error")
                return
            }
            
           
            
        }
    }
    
    task.resume()
}
    func fetchNewsData(url: NSURL, onSuccess:((data: AnyObject) -> Void)?, onFail:((message:String) -> Void)?)
    {    let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            
            if error == nil{
                let res = response as! NSHTTPURLResponse!
                
                switch res.statusCode {
                case 200 :
                    fallthrough
                    
                case 201 :
                    fallthrough
                case 204 :
                    if data != nil {
                        do{
                            
                            var json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                            onSuccess!(data: json)
                            return
                        }
                        catch{
                            onSuccess!(data: data!)
                        }
                    }
                    else{
                        onSuccess!(data: [])
                        return
                    }
                    
                case 401 :
                    onFail!(message: "Invalid Credentials")
                    return
                case 400:
                    onFail!(message: "Incorrect Request Format")
                    return
                default :
                    onFail!(message : "Internal Server Error")
                    return
                }
                
                
                
            }
        }
        
        task.resume()
    }
    class func loadHotelImages(Url:String,completionHandler: (genres: UIImage) -> ()) {
        let url = NSURL(string: Url)
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        var response: NSURLResponse?
        var error : NSError?
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(mydata, response, error) in
            if((mydata) != nil)
            {
                let imageData = UIImage(data:mydata!)
                if((imageData) != nil)
                {
                    //sleep(2)
                    completionHandler( genres: imageData!);
                }
            }
            else
            {
                let image:UIImage = UIImage(named: "news")!;
                completionHandler( genres: image);
            }
            
        }
        
        task.resume()
        
    }
}