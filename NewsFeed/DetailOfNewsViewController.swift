//
//  DetailOfNewsViewController.swift
//  NewsFeed
//
//  Created by Venkatesh on 3/13/16.
//  Copyright © 2016 venkatesh. All rights reserved.
//

import UIKit

class DetailOfNewsViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var link:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        //blurView.hidden = true
        //scrollView.canCancelContentTouches = false
        // Do any additional setup after loading the view.
        self.activityIndicator.hidden = false
        self.activityIndicator.startAnimating()
        let url = NSURL (string: link);
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj);
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        self.activityIndicator.hidden = true
        self.activityIndicator.stopAnimating()
    }
}
