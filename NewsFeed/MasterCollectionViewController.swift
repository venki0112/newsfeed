//
//  MasterCollectionViewController.swift
//  NewsFeed
//
//  Created by Venkatesh on 3/12/16.
//  Copyright © 2016 venkatesh. All rights reserved.
//

import UIKit


class MasterCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {
     var collectionViewCustomLayout: CustomCollectionViewFlowLayout!
    var newspapersTitles = ["The Hindu","Times of India","The Econimic Times","The Indian Express","Miscellaneous"];
     var images = ["Hindu","TimesOfIndia","economicTimes","indianExpress","miscellaneous"];
    var linkstoRSSFeeds = ["http://www.thehindu.com/?service=rss","http://timesofindia.feedsportal.com/c/33039/f/533965/index.rss","http://economictimes.indiatimes.com/news/economy/policy/rssfeeds/1106944246.cms","http://indianexpress.com/print/front-page/feed/","http://ec2-52-32-43-254.us-west-2.compute.amazonaws.com:3000/getNewsHeadlines"];
    var tittle = "";
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewCustomLayout = CustomCollectionViewFlowLayout()
        collectionView!.collectionViewLayout = collectionViewCustomLayout
        self.collectionView!.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundImage")!)
        self.configNavigationBar()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
      
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return newspapersTitles.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        (cell.viewWithTag(1) as! UILabel).adjustsFontSizeToFitWidth = true
        (cell.viewWithTag(1) as! UILabel).text = newspapersTitles[indexPath.row]
        (cell.viewWithTag(2) as! UIImageView).image = UIImage(named: images[indexPath.row])
        cell.layer.borderWidth = 1.0
        cell.layer.cornerRadius = 5.0
        cell.layer.masksToBounds = false
        return cell
    }
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
         return UIEdgeInsetsMake(16, 16, 16, 16);
    }
    
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            if segue.identifier == "showDetail" {
                if let indexPath = self.collectionView?.indexPathsForSelectedItems()?.first {
                    tittle = newspapersTitles[indexPath.row]
                    let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                    controller.pagetitle = tittle
                    controller.link = linkstoRSSFeeds[indexPath.row]
                    controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                    controller.navigationItem.leftItemsSupplementBackButton = true
                }
            }
        }
    func configNavigationBar(){
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        self.navigationItem.backBarButtonItem?.title = ""
      
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationItem.title = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
