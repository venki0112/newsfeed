//
//  NewsDetailsTableViewController.swift
//  NewsFeed
//
//  Created by Venkatesh on 3/12/16.
//  Copyright © 2016 venkatesh. All rights reserved.
//

import UIKit

class NewsDetailsTableViewController: UITableViewController {

    @IBOutlet weak var imageactivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var newsDescription: UITextView!
    @IBOutlet weak var HeadlineTitle: UILabel!
    @IBOutlet weak var NewsImage: UIImageView!
    var imageLink:String!
    var newsdescriptionText:String!
    var newstitle:String!
    var source:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NewsImage.hidden = true
        self.HeadlineTitle.hidden = true
        self.newsDescription.hidden = true
        self.imageactivityIndicator.hidden = false
        self.imageactivityIndicator.startAnimating()
        NetworkHelper.loadHotelImages(imageLink, completionHandler: {
            data in
            dispatch_async(dispatch_get_main_queue(), {
                self.NewsImage.image = data
                self.imageactivityIndicator.hidden = true
            })
        })
        self.configNavigationBar()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
        self.NewsImage.hidden = false
        self.HeadlineTitle.hidden = false
        self.newsDescription.hidden = false
        newsDescription.text = newsdescriptionText
        HeadlineTitle.text = newstitle
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.row == 0 )
        {
            var calculteLabel = UILabel()
            calculteLabel.font = self.HeadlineTitle.font
            calculteLabel.text = self.HeadlineTitle.text
            calculteLabel.numberOfLines = 2
            return   calculteLabel.sizeThatFits(CGSizeMake(self.HeadlineTitle.bounds.width, CGFloat.max)).height + 10
        }
        if(indexPath.row == 1)
        {
            return self.NewsImage.bounds.width * 0.5
        }
        if(indexPath.row == 2)
        {    var calculteTextView = UITextView()
            calculteTextView.font = self.newsDescription.font
            calculteTextView.text = self.newsDescription.text
            return   calculteTextView.sizeThatFits(CGSizeMake(self.newsDescription.bounds.width, CGFloat.max)).height + 10
        }
        return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    func configNavigationBar(){
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        self.navigationItem.backBarButtonItem?.title = ""
        
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationItem.title = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

         Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
