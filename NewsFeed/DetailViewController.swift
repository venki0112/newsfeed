//
//  DetailViewController.swift
//  NewsFeed
//
//  Created by Venkatesh on 3/12/16.
//  Copyright © 2016 venkatesh. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,NSXMLParserDelegate,UIAlertViewDelegate {

    @IBOutlet weak var activityIndicatorforFetchingNewsFeed: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var newsSearchBar: UISearchBar!
    var link = "http://www.thehindu.com/?service=rss";
    var parser = NSXMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var title1 = NSMutableString()
    var Rssdetaillink = NSMutableString()
    var filteredData = [String]();
    var tittle = [String]();
    var links = [String]();
    var imagelinks = [String]();
    var detailText = [String]();
    var pagetitle = "The Hindu";
    var source = [String]();
    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.valueForKey("timeStamp")!.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //filteredData = Headlines
        self.configureView()
       
        self.tableView.hidden = true
        self.activityIndicatorforFetchingNewsFeed.startAnimating()
        if(pagetitle == "Miscellaneous")
        {
            NetworkHelper().fetchNewsData(NSURL(string:link)!, onSuccess: { (data) -> Void in
                dispatch_async(dispatch_get_main_queue(), {
                    do {
                        for newsdata in data as! [NSDictionary]
                        {
                            if let title = newsdata.valueForKey("title") as? String
                            {
                                    self.tittle.append(title)
                            }
                            else
                            {
                                self.tittle.append("")
                            }
                            if let url = newsdata.valueForKey("imageData") as? String
                            {
                                self.imagelinks.append(url)
                            }
                            else
                            {
                                self.imagelinks.append("")
                            }
                            if let detailText = newsdata.valueForKey("detailText") as? String
                            {
                                self.detailText.append(detailText)
                            }
                            else
                            {
                                self.detailText.append("")
                            }
                            if let source = newsdata.valueForKey("source") as? String
                            {
                                self.source.append(source)
                            }
                            else
                            {
                                self.source.append("")
                            }
                        }
                        self.filteredData = self.tittle
                        self.tableView.reloadData()
                        self.activityIndicatorforFetchingNewsFeed.hidden = true
                        self.tableView.hidden = false
                        self.tableView.reloadData()
                    } catch {
                        print(error)
                    }
                
                })
                }) { (message) -> Void in
                    UIAlertView(title: "Failed", message: "Failed To Fetch News", delegate: self, cancelButtonTitle: "OK").show()
            }

        }
        else
        {
            self.beginParsing()
        }
        self.configNavigationBar()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationItem.title = self.pagetitle
    }
    func beginParsing()
    {
        posts = []
        NetworkHelper().fetchData(NSURL(string:link)!, onSuccess: { (data) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
            self.parser = NSXMLParser(data: data as! NSData)
            self.parser.delegate = self
            self.parser.parse()
            self.filteredData = self.tittle
            self.tableView.reloadData()
            self.activityIndicatorforFetchingNewsFeed.hidden = true
            self.tableView.hidden = false
            })
            }) { (message) -> Void in
             UIAlertView(title: "Failed", message: "Failed To Fetch News", delegate: self, cancelButtonTitle: "OK").show()
        }
        
    }
    
    //XMLParser Methods
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName
        if (elementName as NSString).isEqualToString("item")
        {
            elements = NSMutableDictionary()
            elements = [:]
            title1 = NSMutableString()
            title1 = ""
            Rssdetaillink = NSMutableString()
            Rssdetaillink = ""
        }
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if (elementName as NSString).isEqualToString("item") {
            if !title1.isEqual(nil) {
                if(title1 != "")
                {
                    //print(title1)
                tittle.append(title1 as String)
                }
            }
            if !Rssdetaillink.isEqual(nil) {
                if(Rssdetaillink != "")
                {
                links.append(Rssdetaillink as String);
                }
            }
            
           
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String)
    {
        if element.isEqualToString("title") {
            title1.appendString(string.stringByReplacingOccurrencesOfString("^\\s*",
                withString: "", options: .RegularExpressionSearch))
        } else if element.isEqualToString("link") {
            Rssdetaillink.appendString(string.stringByReplacingOccurrencesOfString("^\\s*",
                withString: "", options: .RegularExpressionSearch))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return  1
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        (cell.viewWithTag(1) as! UILabel).text = filteredData[indexPath.row]
        
    }
    // This method updates filteredData based on the text in the Search Box
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(pagetitle == "Miscellaneous")
        {
            let listViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("newsdetails") as! NewsDetailsTableViewController
            listViewController.navigationItem.title = "Source: \(source[indexPath.row])"
            listViewController.imageLink = imagelinks[indexPath.row]
            listViewController.newsdescriptionText = detailText[indexPath.row]
            listViewController.newstitle = tittle[indexPath.row]
            listViewController.source = source[indexPath.row]
            self.navigationController?.pushViewController(listViewController, animated: true)
        }
        else
        {
        let listViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("webView") as! DetailOfNewsViewController
        listViewController.navigationItem.title = filteredData[indexPath.row]
        listViewController.link = links[indexPath.row]
        self.navigationController?.pushViewController(listViewController, animated: true)
        }

    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredData = tittle
        } else {
           
            filteredData = tittle.filter({(dataItem: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                if dataItem.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil {
                    return true
                } else {
                    return false
                }
            })
        }
        self.tableView.reloadData()
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.newsSearchBar.resignFirstResponder()
    }
    func configNavigationBar(){
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
    }

}

